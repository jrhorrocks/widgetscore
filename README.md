# To Run app #

* Clone repository
* set up grails 3.1.4
* from terminal/command prompt navigate to cloned folder and run - grails run-app
+ (there is an in memory database that is pre-seeded with data)
    * http://localhost:8080/dbconsole/login.do?jsessionid=5a1e0599d4914214299695873a7de2e3
+ To get the average
    * http://localhost:8080/rating/averageRating/2
+ To create rating
    * http://localhost:8080/user/rateWidget?userId=1&widgetId=2&rating=4