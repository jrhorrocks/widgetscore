package org.widgetscore

import grails.converters.JSON

class RatingController {
	def ratingService
	
	def averageRating() {
		
		def id = params?.id
		def result
		if (id) {
			result = ratingService.getAverageRatingForWidget(id.toLong())
		} else {
			result = [:]
		}
		
		render result as JSON
	}
}
