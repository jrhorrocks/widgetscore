package org.widgetscore

class WidgetController {

    def save() { 
		def widget = new Widget()
		def id = params?.id
		
		if (id) {
			widget.with {
				id = params.id.toLong()
				name = 'name ' + params.id
			}
			
			widget.save()
		} else {
			render '<h2>Please provide an ID</h2>'
		}
		
		render '<h4>success</h4>'
	}
}
