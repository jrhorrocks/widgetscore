package org.widgetscore

import grails.converters.JSON

class UserController {
	def ratingService
	def userService
	
    def list() { 
		def results = userService.getUsers()
		render results as JSON
	}
	
	def getUserById() {
		def id = params?.id
		def results = [:]
		if (id) {
			results = userService.getUserById(id.toLong())
		}
		
		render results as JSON
	}
	
	def rateWidget() {
		def userId = params?.userId.toLong()
		def widgetId = params?.widgetId.toLong()
		def rating = params?.rating.toInteger()
		
		def results = ratingService.saveRating(userId, widgetId, rating)
		
		render results as JSON
	}
	
	def saveUser() {
		
	}
}
