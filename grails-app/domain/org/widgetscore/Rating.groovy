package org.widgetscore


class Rating {

    static mapping = {
	   version false
    }
   
    static constraints = {
       user blank: false
       widget blank: false
       userRating range: 0..10, blank: false
    }
   
    Long id
    User user
    Widget widget
    int userRating
    Date ratingTime
}
