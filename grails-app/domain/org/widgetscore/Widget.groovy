package org.widgetscore

class Widget {
	static mapping = {
		version false
	}
	
    static constraints = {
		id unique: true
		name blank: false
    }
	
	Long id
	String name
}
