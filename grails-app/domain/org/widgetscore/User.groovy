package org.widgetscore

class User {
	static mapping = {
		version false
	 }
	
    static constraints = {
		userName blank: false
    }
	
	Long id
	String userName
	
}
