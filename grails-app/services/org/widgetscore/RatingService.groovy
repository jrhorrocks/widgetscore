package org.widgetscore

import grails.transaction.Transactional
import java.sql.Timestamp;
import java.time.LocalDateTime

@Transactional
class RatingService {

    def getAverageRatingForWidget(Long id) {
		def average
		def widget = Widget.get(id)
		
		if(widget) {
			def ratings = Rating.findAllByWidget(widget)
			
			def count = ratings.size()
			def sum =  ratings.userRating.sum()
			average = sum / count
		} else {
			average = 'success'
		}
		
		
		
		[average: average]
    }
	
	def saveRating(Long userId, Long widgetId, int score) {
		User user
		Widget widget
		
		if (userId) {
			user = User.get(userId)
		}
		if (widgetId) {
			widget = Widget.get(widgetId)
		}
		
		List results = new ArrayList()
		
		def rating = new Rating(user:user, widget:widget, userRating:score, ratingTime:Timestamp.valueOf(LocalDateTime.now()))
		if (rating.validate()) {
			rating.save(flush:true, failOnError: true)
			
			results.add('success')
		}else{
			rating.errors.allErrors.each {
				results.add(it)
			}
		}
		
		results
	}
}
