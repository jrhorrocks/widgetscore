package org.widgetscore

import grails.transaction.Transactional

@Transactional
class UserService {

    def getUsers() {
		def results = User.list()
		
		[results: results]
    }
	
	def getUserById(Long id) {
		def results = User.get(id)
		
		[results: results]
	}
}
