import org.widgetscore.Rating
import org.widgetscore.User
import org.widgetscore.Widget
import grails.util.Environment

import java.sql.Timestamp;
import java.time.LocalDateTime

class BootStrap {

    def init = { servletContext ->
		
		if (Environment.current == Environment.DEVELOPMENT) {
			// users
			def user1 = new User()
			
			user1.with {
				userName = 'user 1'
			}
			user1.save(flush: true)
			
			def user2 = new User()
			
			user2.with {
				userName = 'user2'
			}
			user2.save(flush: true)
			
			// widgets
			def widget1 = new Widget()
			widget1.with {
				name = 'one'
			}
			widget1.save(flush: true)
			
			def widget2 = new Widget()
			widget2.with {
				name = 'two'
			}
			widget2.save(flush: true)
			
			def widget3 = new Widget()
			widget3.with {
				name = 'three'
			}
			widget3.save(flush: true)
			
			
			// ratings
			def rating1 = new Rating(user:user1, widget:widget1, userRating:3, ratingTime:Timestamp.valueOf(LocalDateTime.now()))
			rating1.save(flush: true)
			
			def rating2 = new Rating(user:user2, widget:widget1, userRating:4, ratingTime:Timestamp.valueOf(LocalDateTime.now()))
			rating2.save(flush: true)
			
			def rating3 = new Rating(user:user1, widget:widget2, userRating:4, ratingTime:Timestamp.valueOf(LocalDateTime.now()))
			rating3.save(flush: true)
			
			def rating4 = new Rating(user:user2, widget:widget2, userRating:1, ratingTime:Timestamp.valueOf(LocalDateTime.now()))
			rating4.save(flush: true)
			
			def rating5 = new Rating(user:user2, widget:widget3, userRating:5, ratingTime:Timestamp.valueOf(LocalDateTime.now()))
			rating5.save(flush: true)
		}
		
    }
    def destroy = {
    }
}
